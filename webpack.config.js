const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const ENV = process.env.NODE_ENV || 'development';

const extractSass = new ExtractTextPlugin({
  filename: "styles.css",
  disable: ENV === "development"
});
module.exports = {
    entry: [
        'whatwg-fetch',
        './app/frontend/entry.js',
        'webpack-hot-middleware/client',
        './app/frontend/index.html',
        './app/frontend/normalize.css'
    ],

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },

    module: {
        rules: [
          {
            test: /\.html|\.css/,
            use:[{
              loader: 'file-loader?name=[name].[ext]'
            }],
          },
          {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /(\.woff|\.ttf|\.svg|\.eot|\.gif)/,
                use: 'resolve-url-loader'
            },
            {
                test: /\.scss$/,
                use: extractSass.extract({
                  use: [{
                    loader: "css-loader"
                  }, {
                    loader: "sass-loader"
                  },{
                    loader: "resolve-url-loader"
                  }],
                  // use style-loader in development
                  fallback: "style-loader"
                })
            }
        ]
    },
    resolve: {
        alias: {
            js: path.resolve(__dirname, 'app/frontend/'),
            components: path.resolve(__dirname, 'app/frontend/components'),
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        extractSass
    ]
}


