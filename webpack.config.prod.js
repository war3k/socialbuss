const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// const ENV = process.env.NODE_ENV || 'development';

const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify("production")
};

const extractSass = new ExtractTextPlugin({
  filename: "styles.css",
  disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: [
        'whatwg-fetch',
        './app/frontend/entry.js',
        './app/frontend/index.html',
        './app/frontend/normalize.css'
    ],

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/'
    },

    module: {
        rules: [
            {
              test: /\.html|\.css/,
              use:[{
                loader: 'file-loader?name=[name].[ext]'
              }],
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader', 'eslint-loader']
            },
            {
                test: /(\.woff|\.ttf|\.svg|\.eot|\.gif)/,
                use: 'url-loader'
            },
            {
              test: /\.scss$/,
              use: extractSass.extract({
                use: [{
                  loader: "css-loader"
                }, {
                  loader: "sass-loader"
                }]
              })
            }
        ]
    },
    resolve: {
      alias: {
        js: path.resolve(__dirname, 'app/frontend/'),
        components: path.resolve(__dirname, 'app/frontend/components'),
      }
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.DefinePlugin(GLOBALS),
        new webpack.optimize.UglifyJsPlugin(),
        extractSass
    ]

}
