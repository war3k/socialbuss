//@flow
import { createActions, handleActions } from "redux-actions";
import INITIAL_STATE from "./initialState";

export type PhotoActions = {
  uploadPhotoAction: any,
  uploadPhotoActionSuccess: any
};

export const actions: PhotoActions = createActions({
  UPLOAD_PHOTO_ACTION:  payload => payload,
  UPLOAD_PHOTO_ACTION_SUCCESS:  payload => payload,
});


const uploadPhotoSuccess = (state, {payload}) => ({...state, src: payload});

export default handleActions(
  {
    [actions.uploadPhotoActionSuccess]: uploadPhotoSuccess,
  },
  INITIAL_STATE.photo
);

export const selectors = {
  getPhotoSrc: ({photo}) => photo && photo.src
};
