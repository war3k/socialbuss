//@flow
import { createActions, handleActions } from "redux-actions";
import initialState from "./initialState";
import _uniqBy from "lodash/uniqBy";

export const INITIAL_STATE = initialState.profiles;

type UserActions = {
  getAllProfilesAction: any,
  getAllProfilesActionSuccess: any,
  getProfileByIdAction:any,
  getProfileByIdActionSuccess:any,
  observeProfileById:any,
  observeProfileByIdSuccess:any
};

export const actions: UserActions = createActions({
  GET_ALL_PROFILES_ACTION: payload => payload,
  GET_ALL_PROFILES_ACTION_SUCCESS: payload => payload,
  GET_PROFILE_BY_ID_ACTION: payload => payload,
  GET_PROFILE_BY_ID_ACTION_SUCCESS: payload => payload,
  OBSERVE_PROFILE_BY_ID_ACTION: payload => payload,
  OBSERVE_PROFILE_BY_ID_ACTION_SUCCESS: payload => payload
});

const getAllProfiles = (state, { payload }) => {
  return {
    ...state,
    data: _uniqBy([ ...payload ], '_id')
  };
};
const getProfileById = (state, { payload }) => {
  return {
    ...state,
    currentProfile: payload
  };
};

export default handleActions(
  {
    [actions.getAllProfilesActionSuccess]: getAllProfiles,
    [actions.getProfileByIdActionSuccess]: getProfileById
  },
  INITIAL_STATE
);
export const selectors = {
  getAllProfiles: ({ profile }): any => profile.data,
  getCurrentProfile: ({profile}): any => profile.currentProfile
};
