//@flow
import { createActions, handleActions } from "redux-actions";
import initialState from "./initialState";

export const INITIAL_STATE = initialState.user;

type UserActions = {
  loginAction: any,
  loginActionSuccess: any,
  getUserInfoActionSuccess: any,
  updateUserAction: any,
  updateUserActionSuccess: any,
  registerAction: any,
  logoutAction: any
};

export const actions: UserActions = createActions({
  LOGIN_ACTION: payload => payload,
  LOGIN_ACTION_SUCCESS: payload => payload,
  GET_USER_INFO_ACTION_SUCCESS: payload => payload,
  UPDATE_USER_ACTION: payload => payload,
  UPDATE_USER_ACTION_SUCCESS: payload => payload,
  REGISTER_ACTION: payload => payload,
  LOGOUT_ACTION: payload => payload
});

const loginSuccess = (state, { payload }) => {
  localStorage.setItem("authToken", payload.token);
  return {
    ...state,
    ...payload
  };
};

const getUserInfoSuccess = (state, { payload }) => {
  return {
    ...state,
    ...payload
  };
};
const updateUserSuccess = (state, { payload }) => {
  return {
    ...state,
    ...payload
  };
};

const logout = () => {
  localStorage.removeItem("authToken");
  return {};
};

export default handleActions(
  {
    [actions.loginActionSuccess]: loginSuccess,
    [actions.getUserInfoActionSuccess]: getUserInfoSuccess,
    [actions.updateUserActionSuccess]: updateUserSuccess,
    [actions.logoutAction]: logout
  },
  INITIAL_STATE
);
export const selectors = {
  //user
  getUser: ({user}): any => user,
  getUserProfile: ({ user }): any => {const {token, ...returnedUser} = user; return returnedUser;}
};
