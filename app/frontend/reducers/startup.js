//@flow
import { createActions, handleActions } from 'redux-actions';

const INITIAL_STATE = {};

export type StartupActions = {
  startupAction: any
};

export const actions: StartupActions = createActions({
  STARTUP_ACTION: () => ({})
});

const startup = () => {
  console.info("startup reducer");
  return {}
};

export default handleActions(
  {
    [actions.startupAction]: startup
  },
  INITIAL_STATE
);
