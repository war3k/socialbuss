//@flow
import { createActions, handleActions } from "redux-actions";
import initialState from "./initialState";
import _uniqBy from "lodash/uniqBy";

export const INITIAL_STATE = initialState.feed;

type PostActions = {
  createPostAction: any,
  createPostActionSuccess: any,
  updatePostAction: any,
  updatePostActionSuccess: any,
  deletePostAction: any,
  deletePostActionSuccess: any,
  getPostByIdAction: any,
  getPostByIdActionSuccess: any,
  getAllPostsAction: any,
  getAllPostsActionSuccess: any,
  getSubscribedPostsAction: any,
  getSubscribedPostsActionSuccess: any
};

export const actions: PostActions = createActions({
  CREATE_POST_ACTION: payload => payload,
  CREATE_POST_ACTION_SUCCESS: payload => payload,
  UPDATE_POST_ACTION: payload => payload,
  UPDATE_POST_ACTION_SUCCESS: payload => payload,
  DELETE_POST_ACTION: payload => payload,
  DELETE_POST_ACTION_SUCCESS: payload => payload,
  GET_POST_BY_ID_ACTION: payload => payload,
  GET_POST_BY_ID_ACTION_SUCCESS: payload => payload,
  GET_ALL_POSTS_ACTION: payload => payload,
  GET_ALL_POSTS_ACTION_SUCCESS: payload => payload,
  GET_SUBSCRIBED_POSTS_ACTION: payload => payload,
  GET_SUBSCRIBED_POSTS_ACTION_SUCCESS: payload => payload
});

const getPostById = (state, { payload }) => {
  return {
    ...state,
    currentPost: payload
  };
};

const getAllPosts = (state, { payload }) => {
  return {
    ...state,
    allPosts:  _uniqBy([ ...payload ], '_id')
  };
};


const getSubscribedPosts = (state, { payload }) => {
  return {
    ...state,
    subscribedPosts: _uniqBy([ ...payload ], '_id')
  };
};

export default handleActions(
  {
    [actions.getPostByIdActionSuccess]: getPostById,
    [actions.getAllPostsActionSuccess]: getAllPosts,
    [actions.getSubscribedPostsActionSuccess]: getSubscribedPosts
  },
  INITIAL_STATE
);
export const selectors = {
  //feed
  getCurrentPost: ({ feed }): any => feed.currentPost,
  getAllPosts: ({ feed }): any => feed.allPosts,
  getSubscribedPosts: ({ feed }): any => feed.subscribedPosts
};
