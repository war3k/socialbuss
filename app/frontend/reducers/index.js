import { combineReducers } from "redux";
import user from "./user";
import feed from "./feed";
import photo from "./photo";
import profile from "./profile";
import { reducer as form } from "redux-form";


const rootReducer = combineReducers({
  user,
  feed,
  photo,
  form,
  profile
});

export default rootReducer;
