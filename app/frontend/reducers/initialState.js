export default {
  user: {},
  form: {},
  feed: {
    currentPost: undefined,
    allPosts: [],
    subscribedPosts: []
  },
  photo:{
    src: undefined
  },
  profiles: {
    data:[]
  }

};
