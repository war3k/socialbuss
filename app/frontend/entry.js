import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';

import { ConnectedRouter } from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';

import { Provider } from 'react-redux';

import App from 'js/components/App';

import configureStore from 'js/store/configureStore';
import rootApiSaga from 'js/sagas/watchers';

const { store, sagaMiddleware } = configureStore();

const history = createHistory();
sagaMiddleware.run(rootApiSaga);

ReactDOM.render(
    <Provider store={store}>
         <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>,
    document.getElementById('app')
);