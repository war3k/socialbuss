import REST from "js/constants/rest";
import API from "js/api/index";


const BASE_URL = "/users";
export default class UserApi {

  static async login(params) {
    const path = '/authenticate';

    const options = {
      method: REST.POST,
      body: JSON.stringify(params)
    };

    return API.fetch(BASE_URL+path, options);
  }

  static async register(params) {
      const path = '/register';

      const options = {
          method: REST.POST,
          body: JSON.stringify(params)
      };

      return API.fetch(BASE_URL+path, options);
  }

  static async getUserInfo() {
    const path = '/info';

    const options = {
      method: REST.GET
    };

    return API.fetch(BASE_URL+path, options);
  }

  static async updateUser(params) {
    const path = `/${params._id}`;
    delete params._id;

    const options = {
      method: REST.PUT,
      body: JSON.stringify(params)
    };

    return API.fetch(BASE_URL+path, options);
  }

}