import REST from "js/constants/rest";
import API from "js/api/index";
import { convertParamsToQuery } from "./utils";


const BASE_URL = "/feed";
export default class UserApi {

  static async getPosts(params = {}) {
    const path = convertParamsToQuery(params);
    const options = {
      method: REST.GET
    };

    return API.fetch(BASE_URL+path, options);
  }


  static async getPostById(_id) {
    const path = `/${_id}`;
    const options = {
      method: REST.GET
    };

    return API.fetch(BASE_URL+path, options);
  }

  static async createPost(params) {

    const options = {
      method: REST.POST,
      body: JSON.stringify(params)
    };

    return API.fetch(BASE_URL, options);
  }

  static async updatePost(params) {
    const path = `/${params._id}`;
    delete params._id;

    const options = {
      method: REST.PUT,
      body: JSON.stringify(params)
    };

    return API.fetch(BASE_URL+path, options);
  }

  static async deletePost(id) {
    const path = `/${id}`;

    const options = {
      method: REST.DELETE
    };

    return API.fetch(BASE_URL+path, options);
  }

}