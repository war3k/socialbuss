const API_URL = '/api';

const authToken = () => localStorage.getItem('authToken');
function requestHeaders() {
  return {
    "Content-Type": "application/json",
    "Authorization" : `Bearer ${authToken()}`
  };
}

async function fetchIt(url, options, headers?) {
  let optionsWithHeaders;
  if(headers === 'file'){
    optionsWithHeaders = {...options, headers:{"Authorization" : `Bearer ${authToken()}`}};
  } else if(!headers) {
    optionsWithHeaders = {...options, headers: requestHeaders()};
  }else{
    optionsWithHeaders = {...options};
  }
  const request = new Request(API_URL+url, optionsWithHeaders);
  let data = {};
  let response = {};

  try {
    response = await fetch(request);

    if (response.status >= 400) throw new Error(response.status);
    data = await _getJsonData(response);
    return data;
  } catch (error) {
    error.data = await _getJsonData(response);
    return Promise.reject(error);
  }
}

async function _getJsonData(response) {
  let result = {};
  const contentType = response.headers.get("content-type");
  if (contentType && contentType.indexOf("application/json") !== -1) {
    try {
      result = await response.json();
    } catch (err) {
      result = {};
    } //workaround for empty response data
  }
  return result;
}

export default {
  fetch: fetchIt
};
