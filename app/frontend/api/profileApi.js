import REST from "js/constants/rest";
import API from "js/api/index";


const BASE_URL = "/users";
export default class ProfileApi {

  static async getProfiles() {

    const options = {
      method: REST.GET
    };

    return API.fetch(BASE_URL, options);
  }
  static async getProfileById(id) {
    const path = `/${id}`;
    const options = {
      method: REST.GET
    };

    return API.fetch(BASE_URL+path, options);
  }
  

}