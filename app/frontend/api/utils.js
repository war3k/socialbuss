export function convertParamsToQuery(params) {
  let query = "?";
  for (let i in params) {
    query += `${i}=${params[i]}&`;
  }
  return query.slice(0, -1);
}
