//@flow
import REST from "js/constants/rest";
import API from "js/api/index";


const BASE_URL = "/file";
export default class FileApi {

  static async uploadFile(file: any): any {
    let data = new FormData();
    data.append("file", file);

    const options = {
      method: REST.POST,
      body: data
    };
    return API.fetch(BASE_URL, options, 'file');
  }

}