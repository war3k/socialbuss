//@flow
import React, { Component, Fragment } from "react";
import {Link, withRouter} from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ProfileEditForm from "js/components/complex/ProfileEditForm";
import {
  actions as profileActions,
  selectors as profileSelectors
} from "js/reducers/profile";
import {
  Grid,
  Header,
  Segment,
  Image,
  Divider,
  Icon,
  Item,
  Container
} from "semantic-ui-react";

type Props = {
  user: any,
  actions: {
    getProfileById: (values: any) => void
  }
};

class Profile extends Component<Props> {
  componentDidMount = () => {
    this.props.actions.getProfileById(this.props.match.params.id);
  };

  render() {
    const { profile } = this.props;
    return (
      <Container>
        {profile && <Grid
          textAlign="left"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column>
           {profile.photo && <Image src={profile.photo} />}
            <Header as="h2" color="teal" textAlign="center">
              <Image src={profile.avatar} avatar />
              {profile.companyName ? profile.companyName : "Profil"}
            </Header>
            <Divider section />
            <Grid columns={2}>
              <Grid.Column>
                <Header color="purple">O nas</Header>
                {profile.description && <p>{profile.description}</p>}
              </Grid.Column>
              <Grid.Column>
                <Header color="purple">Kontakt</Header>
                <p>
                  <Icon name="mail" /> {profile.email}
                  <br />
                  <Icon name="industry" /> {profile.companyName}
                  <br />
                  {profile.address && (
                    <Fragment>
                      <Icon name="address book" />
                      {profile.address.split("\n").map((item, key) => {
                        return (
                          <span key={key}>
                            {item}
                            <br />
                          </span>
                        );
                      })}
                    </Fragment>
                  )}
                </p>
              </Grid.Column>
            </Grid>
          </Grid.Column>
        </Grid>
        }
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: profileSelectors.getCurrentProfile(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        getProfileById: profileActions.getProfileByIdAction
      },
      dispatch
    )
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Profile));
