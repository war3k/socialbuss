//@flow
import React, { Component } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import ProfileEditForm from "js/components/complex/ProfileEditForm";
import { actions as userActions, selectors } from "js/reducers/user";
import { Grid, Header, Segment } from "semantic-ui-react";

type Props = {
  user: any,
  actions: {
    updateUser: (values: any) => void
  }
};

class ProfileEdit extends Component<Props> {
  handleSubmit = values => {
    console.log("vals", values);
    this.props.actions.updateUser(values);
  };
  render() {
    const { user } = this.props;
    return (
      <Segment>
        <Grid
          textAlign="center"
          style={{ height: "100%" }}
          verticalAlign="middle"
        >
          <Grid.Column>
            <ProfileEditForm onSubmit={this.handleSubmit} />
          </Grid.Column>
        </Grid>
      </Segment>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: selectors.getUser(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        updateUser: userActions.updateUserAction
      },
      dispatch
    )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileEdit);
