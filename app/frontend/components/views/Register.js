//@flow
import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import RegisterForm from 'js/components/complex/RegisterForm';
import {actions as userActions} from 'js/reducers/user';
import {Grid, Header} from "semantic-ui-react";

type RegisterProps = {
  actions: {
    registerAction: (values: any) => void
  }
}

class Register extends Component<RegisterProps> {
    handleSubmit = (values) => {
      this.props.actions.registerAction(values);
    };
    render() {
        return (
            <div>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' color='teal' textAlign='center'>
                            Rejestracja:
                        </Header>
                  <RegisterForm onSubmit={this.handleSubmit}/>
                    </Grid.Column>
                </Grid>

            </div>
        )
    }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
          registerAction: userActions.registerAction
      },
      dispatch
    ),
  };
}

export default connect(null,mapDispatchToProps)(Register);
