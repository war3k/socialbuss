//@flow
import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Grid,
  Card,
  Image,
  Button
} from "semantic-ui-react";

class ProfileItem extends Component {
  render() {
    const { profile,handleClick } = this.props;
    return (
      <Grid.Column>
        <Card>
          <Image
            style={{ height: 190 }}
            src={profile.avatar || "/public/files/no-avatar.png"}
          />
          <Card.Content>
            <Card.Header>
              <Link to={`/profile/${profile._id}`}>
                {profile.companyName || profile.email}
              </Link>
            </Card.Header>
            <Card.Description style={{ height: 80 }}>
              {profile.short_description}
            </Card.Description>
            <Card.Meta textAlign="right">
              <Button size="mini" onClick={handleClick} basic color="green">
                Obserwuj
              </Button>
            </Card.Meta>
          </Card.Content>
        </Card>
      </Grid.Column>
    );
  }
}

export default ProfileItem;
