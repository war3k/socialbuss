//@flow
import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import {
  actions as profileActions,
  selectors as profileSelectors
} from "./../../../reducers/profile";
import { Grid, Container, Card, Icon, Image , Button, Header} from "semantic-ui-react";
import ProfileItem from "js/components/views/profileList/ProfileItem";


class ProfileList extends Component {

  componentDidMount = () => {
    this.props.actions.getAllProfiles();
  };
  handleClick = e => {
    this.props.actions.observeProfile(e.target._id);
  };

  render() {
    const { profileList } = this.props;
    return (
      <Container>
        <Grid textAlign="left" style={{ height: "100%" }}>
          <Grid.Row columns={3}>
          
              {profileList &&
                profileList.map(profile => (
                 <ProfileItem profile={profile} handleClick={this.handleClick}/>
                ))}
            
          </Grid.Row>
        </Grid>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    profileList: profileSelectors.getAllProfiles(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        getAllProfiles: profileActions.getAllProfilesAction,
        observeProfile: profileActions.observeProfileById
      },
      dispatch
    )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileList);
