import React, {Component} from 'react';
import {Container, Header} from "semantic-ui-react";


class Main extends Component {


  render() {
    return (
      <Container>
        <Header as="h2" textAlign="center">MAIN PAGE!</Header>
      </Container>
    )
  }
}

export default Main;
