//@flow
import React, { Component } from 'react';
import {Link, Redirect} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import LoginForm from 'js/components/complex/LoginForm';
import {actions as userActions, selectors} from 'js/reducers/user';
import {Grid, Header, Message} from "semantic-ui-react";

type LoginProps = {
  user: any;
  actions: {
    loginAction: (values: any) => void
  }
}

class Login extends Component<LoginProps> {
    state = {
        redirectToReferrer: false
    };
    handleSubmit = (values) => {
      const {actions} = this.props;
      actions.loginAction(values);
      this.setState({redirectToReferrer:true});
    };
    render() {
        const {redirectToReferrer} = this.state;
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        return (
            redirectToReferrer || !!this.props.user.token ? <Redirect to={from}/> :
            <div>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 450 }}>
                        <Header as='h2' color='teal' textAlign='center'>
                            Logowanie:
                        </Header>
                        <LoginForm onSubmit={this.handleSubmit}/>
                        <Message>
                           Nie masz konta? <Link to='register'>Rejestracja</Link>
                        </Message>
                    </Grid.Column>
                </Grid>

            </div>
        )
    }
}

function mapStateToProps(state) {
  return {
    user: selectors.getUser(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
        loginAction: userActions.loginAction
      },
      dispatch),
  };
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
