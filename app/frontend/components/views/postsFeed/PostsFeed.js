//@flow
import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { actions as feedActions, selectors } from "./../../../reducers/feed";
import { selectors as userSelectors } from "./../../../reducers/user";
import {
  Grid,
  Container,
  Card,
  Icon,
  Image,
  Form,
  TextArea,
  Divider,
  Header,
  Modal,
  Button,
  Feed
} from "semantic-ui-react";
import "./PostsFeed.scss";
import AddPostForm from "./../../complex/PostForm";

type Props = {
  user: any,
  actions: {
    getAllPosts: () => void,
    getSuscribedPosts: () => void,
    addPost: (values: any) => void,
    updatePost: (values: any) => void,
    deletePost: (id: string) => void
  }
};
type State = {
  editId: string,
  postText: string,
  postPhoto: string
};

const initialState = {
  editId: "",
  postText: ""
};

class PostsFeed extends Component<Props, State> {
  state = initialState;

  addPost = values => {
    this.props.actions.addPost(values);
  };

  handleChangePostText = e => {
    this.setState({ postText: e.target.value });
  };

  handleUpdatePost = () => {
    const params = {
      _id: this.state.editId,
      text: this.state.postText
    };
    this.props.actions.updatePost(params);
    this.setState(initialState);
  };

  handleCancelUpdatePost = () => {
    this.setState(initialState);
  };

  handleEditClick = post => {
    this.setState({
      editId: post._id,
      postText: post.text
    });
  };

  handleDeletePost = postId => {
    this.props.actions.deletePost(postId);
  };

  renderPostButtons = post =>
    this.props.user._id === post.authorId ? (
      <div className={`post__item-buttons ${post._id === this.state.editId? 'active': ''}`}>
        {post._id === this.state.editId ? (
          <Fragment>
            <Button size="mini" positive icon onClick={this.handleUpdatePost}>
              <Icon name="check" />
            </Button>
            <Button
              size="mini"
              color="blue"
              icon
              onClick={this.handleCancelUpdatePost}
            >
              <Icon name="close" />
            </Button>
          </Fragment>
        ) : (
          <Button
            size="mini"
            icon
            color="teal"
            onClick={() => this.handleEditClick(post)}
          >
            <Icon name="pencil" />
          </Button>
        )}
        <Modal
          basic
          size="tiny"
          trigger={
            <Button size="mini" icon color="blue" inverted>
              <Icon name="trash" />
            </Button>
          }
          header={<Header icon="trash" content="Usunąć post?" />}
          actions={[
            {
              key: "no",
              content: "Nie",
              negative: true
            },
            {
              key: "yes",
              content: "Tak",
              positive: true,
              onClick: () => this.handleDeletePost(post._id)
            }
          ]}
        />
      </div>
    ) : null;

  componentDidMount = () => {
    this.props.actions.getAllPosts();
  };

  render() {
    const { user, subscribedPosts, allPosts } = this.props;
    const { editId,postText } = this.state;  
    return (
      <Container>
        <Grid textAlign="left" style={{ height: "100%" }}>
          <Grid.Row centered>
            <Grid.Column style={{ maxWidth: 600 + "px" }}>
              <AddPostForm onSubmit={this.addPost} />
            </Grid.Column>
          </Grid.Row>
          <Divider />
          {allPosts &&
            allPosts.map((post, i) => (
              <Grid.Row key={i} centered>
                <Grid.Column style={{ maxWidth: 600 + "px" }}>
                  <Card fluid>
                    <Card.Content>
                      <Card.Meta textAlign="right">
                        {this.renderPostButtons(post)}
                        {new Date(post.created).toLocaleDateString()}
                      </Card.Meta>
                      <Card.Header textAlign="left">
                        <Link to={`/profile/${post.authorId}`}>
                          <Image
                            floated="left"
                            src={post.author.avatar}
                            size="mini"
                          />
                          {post.author.companyName || post.author.email}
                        </Link>
                      </Card.Header>
                    </Card.Content>
                    {post.photo && <Image src={post.photo} />}

                    <Card.Content>
                      {editId === post._id ? (
                        <Form>
                          <TextArea
                            placeholder="Tekst..."
                            name="text"
                            value={postText}
                            onChange={this.handleChangePostText}
                          />
                        </Form>
                      ) : (
                        post.text
                      )}
                    </Card.Content>
                    {/* <Card.Content extra>
                      <a>
                        <Icon name="like" />
                        10 Likes
                      </a>
                    </Card.Content> */}
                  </Card>
                </Grid.Column>
              </Grid.Row>
            ))}
        </Grid>
      </Container>
    );
  }
}

function mapStateToProps(state) {
  return {
    subscribedPosts: selectors.getSubscribedPosts(state),
    allPosts: selectors.getAllPosts(state),
    user: userSelectors.getUserProfile(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        getAllPosts: feedActions.getAllPostsAction,
        getSuscribedPosts: feedActions.getSubscribedPostsAction,
        addPost: feedActions.createPostAction,
        updatePost: feedActions.updatePostAction,
        deletePost: feedActions.deletePostAction
      },
      dispatch
    )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostsFeed);
