//@flow
import React, { Component } from "react";
import { Switch, Route, Redirect, BrowserRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { selectors } from "js/reducers/user";
import { actions as startupActions } from "js/reducers/startup";

import "../theme/styles.scss";

import Navbar from "components/common/navbar/Navbar";
import Footer from "components/common/Footer";
import Login from "js/components/views/Login";
import Register from "js/components/views/Register";
import Main from "js/components/views/Main";
import ProfileEdit from "js/components/views/profile/ProfileEdit";
import Profile from "js/components/views/profile/Profile";
import PostsFeed from "./views/postsFeed/PostsFeed";
import ProfileList from "./views/profileList/ProfileList";

type Props = {
  user: any,
  actions: {
    startupAction: any
  }
};

const PrivateRoute = ({ component: Component, user, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        !!user.token ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

class App extends Component<Props> {
  componentDidMount() {
    this.props.actions.startupAction();
  }
  render() {
    const { user } = this.props;
    return (
      <BrowserRouter>
        <div>
          <Navbar user={user} />
          <div className="page-content">
            <Switch>
              <Route path="/login" component={Login} />
              <Route path="/register" component={Register} />
              <PrivateRoute
                user={user}
                exact
                path="/profile"
                component={ProfileEdit}
              />
              <PrivateRoute
                user={user}
                exact
                path="/profile/:id"
                component={Profile}
              />
              <PrivateRoute
                user={user}
                exact
                path="/"
                component={PostsFeed}
                />
                <PrivateRoute
                user={user}
                exact
                path="/profiles"
                component={ProfileList}
                />
            </Switch>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: selectors.getUser(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        startupAction: startupActions.startupAction
      },
      dispatch
    )
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
