// @flow
import React, { Component } from "react";
import {Dropdown, Menu, Icon, Image} from "semantic-ui-react";
import "./Navbar.scss";
import { Link, withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import { actions as userActions } from "js/reducers/user";
import { connect } from "react-redux";

type Props = {
  actions: {
    logoutAction: () => void
  }
};

class Navbar extends Component<Props> {
  state = { activeItem: "Orders" };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  logout = () => {
    this.props.actions.logout();
    this.props.history.replace("/");
  };
  render() {
    const { user } = this.props;
    const { activeItem } = this.state;
    return (
      <nav className="navbar">
        <Menu color="teal" pointing secondary>
          <Menu.Menu position="left">
            <Link to="/">
              <Menu.Item>
               <Image size='mini' src='/public/files/logo.jpg'/>
              </Menu.Item>
            </Link>
          </Menu.Menu>
          <div className="items">
            <Link className="item" to="/">
              <Menu.Item
                as="div"
                name="Tablica"
                active={activeItem === "Tablica"}
                onClick={this.handleItemClick}
              />
            </Link>
            <Link className="item" to="/profiles">
              <Menu.Item
                as="div"
                name="Profile"
                active={activeItem === "Profile"}
                onClick={this.handleItemClick}
              />
            </Link>
          </div>
          <Menu.Menu position="right">
            {Object.keys(user).length ? (
              <Dropdown
                trigger={
                  <span>
                    <Icon name="user" />
                    {user.email}
                  </span>
                }
                item
              >
                <Dropdown.Menu>
                  <Link to="/profile">
                    <Dropdown.Item>Edycja profilu</Dropdown.Item>
                  </Link>
                  <Link to={`/profile/${user._id}`}>
                    <Dropdown.Item>Podgląd profilu</Dropdown.Item>
                  </Link>
                  <Dropdown.Item onClick={this.logout}>Wyloguj</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            ) : (
              <Link className="item" to="/login">
                <Menu.Item
                  as="div"
                  name="Login"
                  active={activeItem === "Login"}
                  onClick={this.handleItemClick}
                />
              </Link>
            )}
          </Menu.Menu>
        </Menu>
      </nav>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        logout: userActions.logoutAction
      },
      dispatch
    )
  };
}
export default withRouter(connect(null, mapDispatchToProps)(Navbar));
