import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import {renderField } from "js/utils/form";
import { required } from "js/utils/validators";
import {Button,Form} from "semantic-ui-react";


class LoginForm extends Component {

  render() {
    const { handleSubmit } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
              <Field
                  fluid
                  icon='user'
                  iconPosition='left'
                  name="email"
                  type="text"
                  placeholder="Email"
                  component={renderField}
                  validate={[required]}
                  tag="input"
              />
              <Field
                  fluid
                  icon='lock'
                  iconPosition='left'
                  name="password"
                  type="password"
                  placeholder="Hasło"
                  component={renderField}
                  validate={[required]}
                  tag="input"
              />
        <Button type="submit">Login</Button>
      </Form>
    );
  }
}

export default reduxForm({
  form: "login",
})(LoginForm);
