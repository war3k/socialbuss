import React, { Component } from "react";
import { Field, Form, reduxForm } from "redux-form";
import {renderField } from "js/utils/form";
import {Button} from "semantic-ui-react";
import {required, password_confirmation, password} from '../../utils/validators';


class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.displayName = "RegisterForm";
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <Form onSubmit={handleSubmit}>
      <div>
          <Field
              fluid
              icon='user'
              iconPosition='left'
              name="email"
              type="text"
              placeholder="Email"
              component={renderField}
              validate={[required]}
              tag="input"
          />
      </div>
      <div>
          <Field
              fluid
              icon='lock'
              iconPosition='left'
              name="password"
              type="password"
              placeholder="Hasło"
              component={renderField}
              validate={[required, password]}
              tag="input"
          />
           <Field
              fluid
              icon='lock'
              iconPosition='left'
              name="password_confirmation"
              type="password"
              placeholder="Powtórz hasło"
              component={renderField}
              validate={[required, password, password_confirmation]}
              tag="input"
          />
      </div>
    <Button type="submit">Dalej</Button>
  </Form>
    );
  }
}

export default reduxForm({
  form: "register",
})(RegisterForm);
