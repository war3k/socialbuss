import React, { Component } from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import { selectors } from "js/reducers/user";
import { renderField, renderFile } from "js/utils/form";
import { required } from "js/utils/validators";
import { Button, Form, Image, Grid, Header,Divider } from "semantic-ui-react";

class ProfileEditForm extends Component {
  render() {
    const { handleSubmit, initialValues } = this.props;
    console.log('render', initialValues);
    return (
      <Form autoComplete="new-password" error onSubmit={handleSubmit}>
        <Image centered src={initialValues.photo} />
        <Header as="h2" color="teal" textAlign="center">
              {initialValues.companyName ? initialValues.companyName : 'Edytuj profil firmowy'}
            </Header>
        <Divider section />
        <Grid columns={2}>
          <Grid.Column>
            <Field
              fluid
              icon="mail"
              iconPosition="left"
              name="email"
              type="text"
              placeholder="Email"
              autoComplete="off"
              component={renderField}
              tag="input"
            />
            <Field
              fluid
              icon="lock"
              iconPosition="left"
              name="currentPassword"
              type="password"
              placeholder="Aktualne hasło ... "
              autoComplete="off"
              component={renderField}
              tag="input"
            />
            <Field
              fluid
              icon="lock"
              iconPosition="left"
              name="newPassword"
              type="password"
              placeholder="Nowe hasło ... "
              autoComplete="off"
              component={renderField}
              tag="input"
            />
            <Field
              fluid
              icon="lock"
              iconPosition="left"
              name="passwordConfirm"
              type="password"
              placeholder="Powtórz hasło ... "
              autoComplete="off"
              component={renderField}
              tag="input"
            />
          </Grid.Column>
          <Grid.Column>
            <Field
              fluid
              icon="industry"
              iconPosition="left"
              name="companyName"
              type="text"
              placeholder="Nazwa firmy"
              autoComplete="off"
              component={renderField}
              tag="input"
            />
            <Field
              fluid
              icon="address book outline"
              iconPosition="left"
              name="address"
              type="textarea"
              rows={7}
              placeholder="Adres"
              autoComplete="off"
              component={renderField}
              tag="textarea"
            />
            <Field
              fluid
              name="short_description"
              type="textarea"
              rows={2}
              placeholder="Krótki opis..."
              autoComplete="off"
              component={renderField}
              tag="textarea"
            />
            <Field
              fluid
              name="description"
              type="textarea"
              rows={7}
              placeholder="Opis firmy..."
              autoComplete="off"
              component={renderField}
              tag="textarea"
            />
            <Field
              fluid
              name="avatar"
              type="file"
              id="avatar"
              customButtonText="Wybierz avatar"
              placeholder="file"
              component={renderFile}
              tag="file"
            />
            <Field
              fluid
              name="photo"
              type="file"
              id="photo"
              customButtonText="Wybierz zdjęcie"
              placeholder="file"
              component={renderFile}
              tag="file"
            />
          </Grid.Column>
        </Grid>
        <Button floated='right' primary type="submit">Zapisz</Button>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  initialValues: selectors.getUserProfile(state)
});

const ProfileEditFormRedux = reduxForm({ form: "profile",enableReinitialize:true })(ProfileEditForm);
export default connect(mapStateToProps, null)(ProfileEditFormRedux);
