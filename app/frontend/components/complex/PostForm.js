import React, { Component } from "react";
import { Field, reduxForm, change } from "redux-form";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { renderTextArea, renderPhotoUpload } from "js/utils/form";
import { required } from "js/utils/validators";
import { Button, Form, Grid } from "semantic-ui-react";
import {
  selectors as photoSelectors,
  actions as photoActions
} from "./../../reducers/photo";
import Divider from "semantic-ui-react/dist/commonjs/elements/Divider/Divider";

class AddPostForm extends Component {
  uploadPhotoAsync = photo => {
    this.props.actions.uploadPhotoAsync(photo);
  };

  componentWillReceiveProps = nextProps => {
    if (!!nextProps.photoSrc) {
      this.props.actions.changeFieldValue("post", "photo", nextProps.photoSrc);
    }
  };

  render() {
    const { handleSubmit, photoSrc } = this.props;
    return (
      <Form onSubmit={handleSubmit} style={{ width: 100 + "%" }}>
        <Grid centered>
          <Grid.Row>
            <Field
              fluid
              name="text"
              type="text"
              placeholder="Czym się dziś pochwalisz?"
              component={renderTextArea}
              validate={[required]}
              tag="textarea"
            />
          </Grid.Row>
          <Grid.Row columns={2}>
            <Grid.Column>
              <Field
                name="photo"
                type="input"
                photoSrc={photoSrc}
                component={renderPhotoUpload}
                asyncUpload={this.uploadPhotoAsync}
                normalize={value => photoSrc}
              />
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Button primary type="submit">
                Dodaj
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Form>
    );
  }
}
const mapStateToProps = state => ({
  photoSrc: photoSelectors.getPhotoSrc(state)
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(
      {
        uploadPhotoAsync: photoActions.uploadPhotoAction,
        changeFieldValue: change
      },
      dispatch
    )
  };
}

const AddPostFormRedux = reduxForm({ form: "post" })(AddPostForm);
export default connect(mapStateToProps, mapDispatchToProps)(AddPostFormRedux);
