//@flow
import React, { Component } from "react";
import {
  Checkbox,
  Dropdown,
  Form,
  Input,
  Icon,
  Radio,
  Image,
  TextArea,
  Button,
  Label
} from "semantic-ui-react";

function getField(tag, props) {
  switch (tag) {
    case "input":
      return renderInput(props);
    case "textarea":
      return renderTextArea(props);
    case "checkbox":
      return renderCheckbox(props);
    case "radio":
      return renderRadio(props);
    case "dropdown":
      return renderDropDown(props);
    case "file":
      return renderFile(props);
    default:
      return renderInput(props);
  }
}

export function renderField({
  children,
  customLabel,
  tag,
  meta: { touched, error },
  ...rest
}: any) {
  return (
    <Form.Field>
      {!!customLabel && <label>{customLabel}</label>}
      {getField(tag, rest)}
      {touched && error && <span className="error-code">{error}</span>}
      {children}
    </Form.Field>
  );
}

export const renderInput = ({
  input,
  placeholder,
  id,
  className,
  type,
  pattern,
  autoFocus,
  maxLength = 100,
  ...rest
}: any) => (
  <Input
    {...input}
    {...rest}
    placeholder={placeholder}
    type={type}
    id={id}
    autoComplete="off"
    className={className}
    maxLength={maxLength}
    pattern={pattern}
    autoFocus={autoFocus}
  />
);

export const renderTextArea = ({
  input,
  placeholder,
  id,
  className,
  type,
  rows = 7,
  maxLength = 500
}: any) => (
  <TextArea
    {...input}
    placeholder={placeholder}
    id={id}
    className={className}
    maxLength={maxLength}
    rows={rows}
  />
);

export const renderDropDown = ({
  input,
  placeholder,
  id,
  className,
  type,
  customPropertyValue,
  options,
  callback,
  loading
}: any) => (
  <Dropdown
    selection
    compact
    {...input}
    value={customPropertyValue ? input.value[customPropertyValue] : input.value}
    id={id}
    className={className}
    placeholder={placeholder}
    onChange={(param, data) => {
      input.onChange(data.value);
      setTimeout(() => callback && callback(data.value));
    }}
    options={options}
    loading={loading}
  />
);

export const renderCheckbox = ({
  input,
  label,
  value,
  disabled,
  ...rest
}: any) => {
  return (
    <Checkbox
      label={label}
      checked={!!input.value}
      disabled={disabled}
      onChange={(param, data) => {
        input.onChange(data.checked);
      }}
      {...rest}
    />
  );
};

export const renderRadio = ({ input, label }: any) => {
  return (
    <Radio
      name={input.name}
      value={input.value}
      label={label}
      checked={input.checked}
      onChange={(param, data) => {
        input.onChange(data.value);
      }}
    />
  );
};

export class renderFile extends Component {
  state = {
    fileName: ""
  };
  render = () => {
    const { fileName } = this.state;
    const {
      customButtonText,
      input,
      id,
      className,
      meta: { touched, error }
    } = this.props;
    delete input.value;
    return (
      <Form.Field className="file">
        <Button floated="left" type="button">
          <label htmlFor={`file-input-${id}`}>
            {customButtonText ? customButtonText : "Wybierz"}
            <Input
              style={{ display: "none" }}
              {...input}
              type="file"
              id={`file-input-${id}`}
              className={className}
              onChange={e => {
                this.setState({ fileName: e.target.files[0].name });
                return input.onChange(e.target.files[0]);
              }}
            />
          </label>
        </Button>
        <span>{fileName}</span>
        {touched && error && <span className="error-code">{error}</span>}
      </Form.Field>
    );
  };
}

export class renderPhotoUpload extends Component {
  state = {
    fileName: ""
  };
  render = () => {
    const { fileName } = this.state;
    const {
      asyncUpload,
      photoSrc,
      customButtonText,
      input,
      id,
      className,
      meta: { touched, error }
    } = this.props;
    delete input.value;
    return (
      <Form.Field className="file">
        <label htmlFor={`file-input-${id}`}>
          {photoSrc ? <Image
            src={photoSrc || ''}
            size="medium"
            rounded={"true"}
          />:<Label size='large'>
          <Icon name="image"/>Dodaj zdjęcie
        </Label>}
          <Input
            style={{ display: "none" }}
            {...input}
            type="file"
            id={`file-input-${id}`}
            className={className}
            onChange={e => {
              this.setState({ fileName: e.target.files[0].name });
              asyncUpload(e.target.files[0]);
              return input.onChange(photoSrc || e.target.files[0]);
            }}
          />
        </label>
        <span>{fileName}</span>
        {touched && error && <span className="error-code">{error}</span>}
      </Form.Field>
    );
  };
}

export const focusOnInput = (elem: any) => {
  return elem.focus();
};
