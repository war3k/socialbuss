
export const required = value => value ? undefined : "Wymagane!";
export const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Email jest niepoprawny!" : undefined;

export const number = value => (value && isNaN(Number(value))) ? "Musi byc cyfrą!" : undefined;
export const password = value => (value && value.length<4) ? "Co najmniej 4 znaki!" : undefined;
export const password_confirmation = (value,allValues) => allValues.password_confirmation !== allValues.password ? "Hasła nie są identyczne!" : undefined;