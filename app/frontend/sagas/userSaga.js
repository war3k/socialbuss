//@flow
import { takeEvery, call, put } from "redux-saga/effects";
import { setSubmitFailed, stopSubmit } from "redux-form";
import UserApi from "js/api/userApi";
import FileApi from "js/api/fileApi";
import { actions as userActions } from "js/reducers/user";

export function* registerSaga({ payload }: { payload: any }): any {
  try {
    const response = yield call(UserApi.register, payload);
    yield put(stopSubmit("register"));
  } catch (error) {
    console.log(error);
    yield put(setSubmitFailed("register"));
  }
}

export function* loginSaga({ payload }: { payload: any }): any {
  try {
    const response = yield call(UserApi.login, payload);
    yield put(stopSubmit("login"));
    yield put(userActions.loginActionSuccess(response));
  } catch (error) {
    console.log(error);
    yield put(setSubmitFailed("login"));
  }
}

export function* updateUserSaga({ payload }: { payload: any }): any {
  try {
    if (payload.photo && typeof payload.photo !== 'string') {
      const photo = yield call(FileApi.uploadFile, payload.photo);
      payload.photo = photo.src.replace(/\\/g, "/");
    }
    if (payload.avatar && typeof payload.avatar !== 'string') {
        const avatar = yield call(FileApi.uploadFile, payload.avatar);
        payload.avatar = avatar.src.replace(/\\/g, "/");
      }
    const response = yield call(UserApi.updateUser, payload);
    yield put(stopSubmit("profile"));
    yield put(userActions.updateUserActionSuccess(response));
  } catch (error) {
    console.log(error);
    yield put(setSubmitFailed("profile"));
  }
}

export function* watchForUserActions() {
  yield takeEvery(userActions.registerAction, registerSaga);
  yield takeEvery(userActions.loginAction, loginSaga);
  yield takeEvery(userActions.updateUserAction, updateUserSaga);
}
