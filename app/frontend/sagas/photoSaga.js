//@flow
import {call, put, takeEvery} from 'redux-saga/effects';
import fileApi from 'js/api/fileApi';
import {actions} from "js/reducers/photo";

export function* uploadFileSaga({ payload }: { payload: any }): any {
  try {
      const response = yield call(fileApi.uploadFile, payload );
      const {src} = response;
      yield put(actions.uploadPhotoActionSuccess(src));
    } catch(error) {
        console.log(error);
    }
}


export function* watchForPhotoActions() {
    yield takeEvery(actions.uploadPhotoAction, uploadFileSaga);
}
