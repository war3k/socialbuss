import { all } from "redux-saga/effects";
import { watchForUserActions } from "js/sagas/userSaga";
import { watchForFeedActions } from "js/sagas/feedSaga";
import { watchForStartup } from "js/sagas/startupSaga";
import { watchForPhotoActions } from "js/sagas/photoSaga";
import { watchForProfileActions } from "js/sagas/profileSaga";

export default function* rootApiSaga() {
  yield all([
    watchForStartup(), 
    watchForUserActions(), 
    watchForFeedActions(),
    watchForPhotoActions(),
    watchForProfileActions()
  ]);
}
