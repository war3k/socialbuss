//@flow
import { takeLatest, call, put } from 'redux-saga/effects';
import UserApi from 'js/api/userApi';
import {actions as startupActions} from "js/reducers/startup";
import {actions as userActions} from "js/reducers/user";


export function* startupSaga(): any {
  try {
        const user = yield call(UserApi.getUserInfo);
        yield put(userActions.getUserInfoActionSuccess(user));
    } catch(error) {
        console.log(error);
    }
}

export function* watchForStartup() {
    yield takeLatest(startupActions.startupAction, startupSaga);
}

