//@flow
import { takeEvery, call, put } from "redux-saga/effects";
import { setSubmitFailed, stopSubmit } from "redux-form";
import feedApi from "js/api/feedApi";
import fileApi from "js/api/fileApi";
import { actions as feedActions } from "js/reducers/feed";

export function* getAllPostsSaga(): any {
  try {
    const response = yield call(feedApi.getPosts);
    yield put(feedActions.getAllPostsActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* getSubscribedPostsSaga(): any {
  try {
    const params = {subscribed: true};
    const response = yield call(feedApi.getPosts, params);
    yield put(feedActions.getSubscribedPostsActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* getPostByIdSaga({payload}): any {
  try {
    const response = yield call(feedApi.getPostById, payload);
    yield put(feedActions.getPostByIdActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* addPostSaga({ payload }): any {

  console.log('post saga', payload);
  try {
    const response = yield call(feedApi.createPost, payload);
    yield put(stopSubmit("post"));
    yield put(feedActions.createPostActionSuccess(response));
  } catch (error) {
    console.log(error);
    yield put(setSubmitFailed("post"));
  }
}

export function* updatePostSaga({ payload }): any {
  try {
    const response = yield call(feedApi.updatePost, payload);
    yield put(feedActions.updatePostActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* deletePostSaga({ payload }): any {
  try {
    const response = yield call(feedApi.deletePost, payload);
    yield put(feedActions.deletePostActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* watchForFeedActions() {
  yield takeEvery(feedActions.getSubscribedPostsAction , getSubscribedPostsSaga);
  yield takeEvery(feedActions.getAllPostsAction, getAllPostsSaga);
  yield takeEvery(feedActions.getPostByIdAction, getPostByIdSaga);
  yield takeEvery(feedActions.createPostAction, addPostSaga);
  yield takeEvery(feedActions.updatePostAction, updatePostSaga);
  yield takeEvery(feedActions.deletePostAction, deletePostSaga);
  yield takeEvery(feedActions.createPostActionSuccess, getAllPostsSaga);
  yield takeEvery(feedActions.updatePostActionSuccess, getAllPostsSaga);
  yield takeEvery(feedActions.deletePostActionSuccess, getAllPostsSaga);
}
