//@flow
import { takeEvery, call, put } from "redux-saga/effects";
import profileApi from "js/api/profileApi";
import { actions as profileActions } from "js/reducers/profile";

export function* getAllProfilesSaga(): any {
  try {
    const response = yield call(profileApi.getProfiles);
    yield put(profileActions.getAllProfilesActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* getProfileByIdSaga({payload}): any {
  try {
    const response = yield call(profileApi.getProfileById, payload);
    yield put(profileActions.getProfileByIdActionSuccess(response));
  } catch (error) {
    console.log(error);
  }
}

export function* watchForProfileActions() {
  yield takeEvery(profileActions.getAllProfilesAction, getAllProfilesSaga);
  yield takeEvery(profileActions.getProfileByIdAction, getProfileByIdSaga);
}
