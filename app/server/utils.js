const config = require('./config.json');
const jwt = require('jsonwebtoken');

module.exports = {
  getUserIdFromAuth: authorization => {
    return new Promise((resolve,reject) => {
      const auth = authorization.split(' ')[1];
      let decoded;
      try {
        decoded = jwt.verify(auth, config.secret);
        resolve(decoded.sub);
      } catch (e) {
        reject('Unathorized', decoded);
      }
    });
  }
};