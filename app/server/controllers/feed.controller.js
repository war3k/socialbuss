const utils = require("./../utils");
const feedService = require("./../services/feed.service");
const express = require("express");
const router = express.Router();

// routes
router.post("/", createPost);
router.get("/", getAll);
router.get("/:id", getById);
router.put("/:id", update);
router.delete("/:id", _delete);

async function createPost(req, res) {
  try {
    const userId = await utils.getUserIdFromAuth(req.headers.authorization);
    const created = await feedService.create(req.body, userId);
    res.status(200).send(created);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function getAll(req, res) {
  try {
    const posts = await feedService.getAll(req.query);
    res.send(posts);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function getById(req, res) {
  try {
    const userId = await utils.getUserIdFromAuth(req.headers.authorization);
    const post = await feedService.getById(req.params.id);
    if (post) {
      res.send(post);
    } else {
      res.sendStatus(404);
    }
  } catch (err) {
    res.status(400).send(err);
  }
}


async function update(req, res) {
  try {
    const updated = await feedService.update(req.params.id, req.body);
    console.log(updated);
    res.status(200).send(updated);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function _delete(req, res) {
  try {
    const deleted = await feedService._delete(req.params.id);
    res.sendStatus(200);
  } catch (err) {
    res.status(400).send(err);
  }
}

module.exports = router;
