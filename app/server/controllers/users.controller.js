const usersService = require("./../services/users.service");
const utils = require("./../utils");
const express = require("express");
const router = express.Router();

// routes
router.post("/authenticate", authenticate);
router.post("/register", register);
router.get("/", getAll);
router.get("/subscribed", getSubscribed);
router.get("/:id", getById);
router.get("/info", getCurrent);
router.put("/:id", update);
router.delete("/:id", _delete);

async function authenticate(req, res) {
  try {
    const user = await usersService.authenticate(req.body);
    res.send(user);
  } catch (err) {
    res.status(401).send(err);
  }
}

async function register(req, res) {
  try {
    const created = await usersService.create(req.body);
    res.sendStatus(200);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function getAll(req, res) {
  try {
    const users = await usersService.getAll();
    res.send(users);
  } catch (err) {
    res.status(400).send(err);
  }
}
async function getSubscribed(req, res) {
  try {
    const userId = await utils.getUserIdFromAuth(req.headers.authorization);
    const users = await usersService.getSubscribed(userId);
    res.send(users);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function getById(req, res) {
  try {
    let user;
    if (req.params.id === "info") {
      const userId = await utils.getUserIdFromAuth(req.headers.authorization);
      user = await usersService.getById(userId);
    } else {
      user = await usersService.getById(req.params.id);
    }
    if (user) {
      res.send(user);
    } else {
      res.sendStatus(404);
    }
  } catch (err) {
    res.status(400).send(err);
  }
}

async function getCurrent(req, res) {
  try {
    const user = await usersService.getById(req.user.sub);
    if (user) {
      res.send(user);
    } else {
      res.sendStatus(404);
    }
  } catch (err) {
    res.status(400).send(err);
  }
}

async function update(req, res) {
  try {
    const updated = await usersService.update(req.params.id, req.body);
    res.status(200).send(updated);
  } catch (err) {
    res.status(400).send(err);
  }
}

async function _delete(req, res) {
  try {
    const deleted = await usersService._delete(req.params.id);
    res.sendStatus(200);
  } catch (err) {
    res.status(400).send(err);
  }
}

module.exports = router;
