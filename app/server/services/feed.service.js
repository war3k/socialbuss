const config = require("../config.json");
const mongo = require("mongoskin");
const db = mongo.db(config.connectionString, { native_parser: true });
const userService = require("./users.service");
db.bind("posts");

const postsService = {
  getAll,
  getById,
  create,
  update,
  _delete
};

function getAll(query) {
  if(query) {
    console.log(query);
    return new Promise((resolve, reject) =>
      db.posts.find(query).toArray(async function (err, posts) {
        if (err) reject({error: err.name + ": " + err.message});
        if(posts){
          for (let post of posts){
           try{
            const user = await userService.getInfoForPostById(post.authorId);
            post.author = user;
           } catch(err){
             console.error(JSON.stringify(err));
           }
          }
          resolve(posts);
        }
        
      })
    );
  }else{
    return new Promise((resolve, reject) =>
      db.posts.find().toArray(function (err, posts) {
        if (err) reject({error: err.name + ": " + err.message});
        resolve(posts);
      })
    );
  }
}

function getById(_id) {
  return new Promise((resolve, reject) =>
    db.posts.findById(_id, function(err, post) {
      if (err) reject({ error: err.name + ": " + err.message });
      if (post) {
        resolve(post);
      } else {
        reject({ error: "post not found!" });
      }
    })
  );
}

async function create(postParams, userId) {
  postParams.authorId = userId;
  postParams.created = new Date();
  return new Promise((resolve, reject) =>
    db.posts.insert(postParams, function(err, result) {
      if (err) reject({ error: err.name + ": " + err.message });
      resolve(result);
    })
  );
}

function update(_id, postParams) {
  return new Promise((resolve, reject) =>
      db.posts.update({ _id: mongo.helper.toObjectID(_id) }, { $set: postParams }, function(err) {
        if (err) reject({ error: err.name + ": " + err.message });
        db.posts.findById(_id, function (err,post) {
          if (err) reject({ error: err.name + ": " + err.message });
          resolve(post);
        })
      }));
}

function _delete(_id) {
  return new Promise((resolve, reject) =>
    db.posts.remove({ _id: mongo.helper.toObjectID(_id) }, function(err) {
      if (err) reject({ error: err.name + ": " + err.message });
      resolve();
    })
  );
}

module.exports = postsService;
