const config = require('../config.json');
const jwt = require('jsonwebtoken');
const omit = require('lodash').omit;
const bcrypt = require('bcryptjs');
const mongo = require('mongoskin');
const db = mongo.db(config.connectionString, { native_parser: true });
db.bind('users');
const userService = {
  authenticate,
  getAll,
  getSubscribed,
  getById,
  getInfoForPostById,
  create,
  update,
  _delete
};

function authenticate(data) {
  return new Promise((resolve,reject) => db.users.findOne({ email: data.email }, function(err, user) {
        if (err) reject({error:err.name + ': ' + err.message});

        if (user && bcrypt.compareSync(data.password, user.hashedPassword)) {
            // authentication successful
            resolve({
              _id: user._id,
              email: user.email,
              companyName: user.companyName,
              address: user.address,
              token: jwt.sign({ sub: user._id }, config.secret),
              photo: user.photo,
              avatar: user.avatar,
              description: user.description
            });
        } else if (!user) {
            // cannot find user
            reject({error:'Nie można znaleźć użytkownika!'});
        } else {
            //authentication failed
            reject({error:'Login lub hasło zawiera błąd!'})
        }
    }));
}

function getAll() {
  return new Promise((resolve,reject) => db.users.find().toArray(function (err, users) {
      if (err) reject({error: err.name + ': ' + err.message});

      // return users (without hashed passwords)
      users = users.map(function(user) {
          return omit(user, 'hashedPassword');
      });

      resolve(users);
    }));
  }

  async function getSubscribed(userId) {
    const user = await getById(userId);
    return new Promise((resolve,reject) => db.users.find({ _id : { $in : user.subscribed}}).toArray(function (err, users) {
        if (err) reject({error: err.name + ': ' + err.message});
  
        // return users (without hashed passwords)
        users = users.map(function(user) {
            return omit(user, 'hashedPassword');
        });
  
        resolve(users);
      }));
    }


  function getById(_id) {
    return new Promise((resolve,reject) => db.users.findById(_id, function (err, user) {
      if (err) reject({error: err.name + ': ' + err.message});

      if (user) {
        // return user (without hashed password)
        resolve({
          _id: user._id,
          email: user.email,
          companyName: user.companyName,
          address: user.address,
          token: jwt.sign({ sub: user._id }, config.secret),
          photo: user.photo,
          avatar: user.avatar,
          description: user.description
        });
      } else {
        // user not found
        reject({error: 'User not found!'});
      }
    }));
  }


  function getInfoForPostById(_id) {
    return new Promise((resolve,reject) => db.users.findById(_id, function (err, user) {
      if (err) reject({error: err.name + ': ' + err.message});

      if (user) {
        // return user (without hashed password)
        resolve({
          email: user.email,
          companyName: user.companyName,
          avatar: user.avatar
        });
      } else {
        // user not found
        reject({error: 'User not found!'});
      }
    }));
  }

function create(userParam) {
    return new Promise((resolve,reject) => // validation
    db.users.findOne({email: userParam.email},
      function (err, user) {
        console.log('user', JSON.stringify(userParam));
        if (err) reject({error: err.name + ': ' + err.message});

        if (user) {
          // email already exists
          reject({error: 'Login "' + userParam.email + '" already exists!'});
        } else {
          createUser(resolve,reject);
        }
      }));

    function createUser(resolve,reject) {
      // set user object to userParam without the cleartext password
      let user = omit(userParam, 'password');

      // add hashed password to user object
      user.hashedPassword = bcrypt.hashSync(userParam.password, 10);

      db.users.insert(
        user,
        function (err, doc) {
          if (err) reject({error: err.name + ': ' + err.message});

          resolve();
        });
    }
  }


function update(_id, userParam) {
    return new Promise((resolve,reject) => // validation
    db.users.findById(_id, function (err, user) {
      if (err) reject({error: err.name + ': ' + err.message});
      if(userParam.currentPassword && !bcrypt.compareSync(userParam.currentPassword, user.hashedPassword)){
        reject({error: "Aktualne hasło jest niepoprawne!"});
      }

      if (user.email !== userParam.email) {
        // email has changed so check if the new email is already taken
        db.users.findOne({email: userParam.email},
          function (err, user) {
            if (err) reject({error: err.name + ': ' + err.message});

            if (user) {
              // email already exists
              reject({error: 'Email: "' + userParam.email + '" jest już zajęty!'})
            } else {
              updateUser(resolve,reject);
            }
          });
      } else {
        updateUser(resolve,reject);
      }
    }));

    function updateUser(resolve,reject) {

      if (userParam.password) {
        userParam.hashedPassword = bcrypt.hashSync(userParam.password, 10);
        delete userParam.password;
      }
      db.users.update({_id: mongo.helper.toObjectID(_id)}, {$set: userParam},
        function (err) {
          if (err) reject({error: err.name + ': ' + err.message});
          db.users.findById(_id, function (err, user) {
            if (err) reject({error: err.name + ': ' + err.message});
      
            if (user) {
              // return user (without hashed password)
              resolve({
                _id: user._id,
                email: user.email,
                companyName: user.companyName,
                address: user.address,
                token: jwt.sign({ sub: user._id }, config.secret),
                photo: user.photo,
                avatar: user.avatar,
                description: user.description,
                subscribed: user.subscribed
              });
            }
          });
        });
    }
  }

function _delete(_id) {
    return new Promise((resolve,reject) => db.users.remove({_id: mongo.helper.toObjectID(_id)},
      function (err) {
        if (err) reject({error: err.name + ': ' + err.message});
        resolve();
      }));

  }

module.exports = userService;
