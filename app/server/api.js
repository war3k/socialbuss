const router = require("express").Router();
const multer = require("multer");
const bodyParser = require("body-parser");
const config = require('./config');
const expressJwt = require('express-jwt');
const bcrypt = require('bcryptjs');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public/files/");
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});

const upload = multer({ storage: storage });

 
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const usersRouter = require('./controllers/users.controller');
const feedRouter = require('./controllers/feed.controller');

router.use("/users", usersRouter);
router.use("/feed", feedRouter);
router.post("/file",urlencodedParser, upload.single("file"), file);

function file(req, res) {
  const response = {
    message: "File uploaded successfully",
    filename: bcrypt.hashSync(req.file.originalname, 10),
    src: "/" + req.file.path.replace(/\\/g, "/")
  };
  res.header("Content-Type", "application/json");
  res.status(200).send(response);
}


module.exports  = router;
